CREATE TABLE relays (
  url TEXT PRIMARY KEY, 
  nips TEXT, 
  created INTEGER, 
  lastseen INTEGER, 
  lastdown INTEGER, 
  uptime INTEGER
);