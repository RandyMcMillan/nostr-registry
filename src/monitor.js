const WebSocket = require('ws')
const db = require('./db') 

class Monitor {
  constructor() {
    /*
      TODO: need to not try to connect so frequently after many failures
      TODO: migrate to nostr-tools once figure out how with node
        subscribe to self and publish to verify actual relay
    */
    this.relays = {}
    this.pending ={}
    this.db = db
    this.db.getRelays().then(relays => {
      this.relays = relays.reduce((o, r) => {
        const {nips, created, lastseen, lastdown, uptime} = r
        o[r.url] = {nips, created, lastseen, lastdown, uptime}
        const p = promiseFunction()
        this.pending[r.url] = p
    
        return o
      },{})
      Object.keys(this.relays).forEach(url => {
        this.connectRelay(url)
      })
    }).catch(console.log)
    setInterval(() => {
      Object.keys(this.relays).forEach(r => {
        const relay = this.relays[r]
        if (relay.ws.readyState !== WebSocket.OPEN && ((now() - relay.lastdown) > 60000)) {
          this.connectRelay(r)
        }

        if (relay.ws.readyState === WebSocket.OPEN) {
          const present = now()
          relay.uptime += (present - (relay.lastseen || relay.created))
          relay.lastseen = present
          this.db.updateRelay(relay.url, relay)
        }
      })
    }, 60000)
  }

  addRelay(url, nips) {
    if (url in this.relays) {
      console.log(`${url} already entered`)
      return new Promise((resolve, reject) => reject('duplicat entry'))
    }
    const relay = {
      nips,
      created: now(),
      lastseen: 0,
      lastdown: 0,
      uptime: 0
    }

    this.relays[url] = relay
    const p = promiseFunction(true)
    this.pending[url] = p
    this.db.addRelay(url, relay).then(() => {
      this.connectRelay(url)
    }).catch (e => {
      this.reject(url)
    })
    return p.promise
  }

  connectRelay(url) {
    const relay = this.relays[url]

    relay.ws = new WebSocket(url)

    relay.ws.onopen = (e) => {
      console.log('connected to', url)
      this.resolve(url)
    }

    relay.ws.onerror = err =>  {
      console.log('error connecting to relay', url, err.message)
      this.reject(url)
    }

    relay.ws.onclose = () => {
      console.log('relay connection closed', url)
      relay.lastdown = now()
      this.db.updateRelay(url, relay)
    }
    
    relay.ws.onmessage = e => {

    }

  }

  getRelays() {
    return Object.keys(this.relays).reduce((o, r) => { // strip out ws object
      const relay = this.relays[r]
      const {created, lastseen, lastdown, uptime, nips} = relay
      const present = now()
      const current = (present - lastseen) > 70000 ? present : lastseen
      const connected = relay.ws.readyState === WebSocket.OPEN
      o[r] = {connected, created, nips, lastseen, lastdown, uptime, uppct: Math.round(100*(uptime/(current - created)))}
      return o
    },{})
  }

  resolve(url) {
    if (url in this.pending) {
      const relay = this.relays[url]
      const present = now()
      // on valid connection we assume it has been up while we were down
      relay.uptime += (present - (relay.lastseen || relay.created))
      relay.lastseen = present
      this.db.updateRelay(url, relay)
      const {nips, created, lastseen, lastdown, uptime} = relay
      this.pending[url].resolve({nips, created, lastseen, lastdown, uptime})
      delete(this.pending[url])
    }
  }

  reject(url) {
    if (url in this.pending) {
      const p = this.pending[url]
      if (p.isNew) p.reject(`error connecting to ${url}`)
      delete(this.pending[url])
    }
  }
}

function now() { return (new Date()).getTime()}

function promiseFunction (isNew) {
  let resolve, reject
  const p =  new Promise((res, rej) => {
    resolve = res;
    reject = rej
  })
  return {
    resolve,
    reject,
    isNew,
    promise: p
  }
}

module.exports = Monitor
