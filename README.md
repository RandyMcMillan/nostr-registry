This is a relay status monitor for relays in the [nostr](https://github.com/fiatjaf/nostr) protocol network.  Relay entry is manual, if you would like to add a relay you can request in the telegram group or via the email in my profile for this repo.

Live [demo](https://moonbreeze.richardbondi.net/nostr-registry/)

Temporary single api endpoint GET https://moonbreeze.richardbondi.net/nostr-registry/api/getrelays

The api may change to include filters but the network is currently small so this should get you started for experimentation.  The api and registry will move to a permanant home at some time in the future as the network grows.